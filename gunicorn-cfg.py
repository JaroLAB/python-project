# -*- encoding: utf-8 -*-
"""
Copyright (c) 2024 - present Seavices
"""

bind = '0.0.0.0:8000'
workers = 2
accesslog = '-'
loglevel = 'debug'
capture_output = True
enable_stdio_inheritance = True
