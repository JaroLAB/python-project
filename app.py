from flask import Flask ,render_template

app = Flask(__name__)

#A continuacion tenemos un decorador que indica que la funcion hello_world()
# se ejecutara cuando se acceda a la ruta raiz de la aplicacion

@app.route('/')
def hello_word():
    return 'Hello, World!'

@app.route('/login')
def login():
    return ("<title> Probando Flask </title>"
            "   <h1> Bienvenido a Flask </h1>"
            "   <p> Flask es un microframework para Python </p>"
            '   <p style="display: flex; align-content: center; align-items: center; justify-content: center;" >'
            "       Parrafo editado desde el codigo fuente de la aplicacion 2"
            "   </p> ")

#Recogemos el parametro que se pasa por la url
@app.route('/home/<string:name>')
def home(name):
    return (f'<h1 style= "font-weight: strong; font-size: 4em">Hello, {name}</h1>')
#Usamos el metodo render_template para renderizar una plantilla html
@app.route('/casa')
@app.route('/')
def casa():
    return render_template('index.html')

@app.route('/form')
def form():
    return render_template('form.html')

@app.route('/home')
def home():
    return render_template('home.html')
if __name__ == '__main__':
    app.run(debug=True)