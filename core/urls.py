# -*- encoding: utf-8 -*-
"""
Copyright (c) 2024 - present julian.com
"""

from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from apps.keenthemes.views import SystemView

urlpatterns = [
    path('admin/', admin.site.urls),

    # Dashboard urls
    path('', include('apps.dashboards.urls')),

    # Auth urls
    path('', include('apps.auth.urls')),
]

handler404 = SystemView.as_view(template_name = 'pages/' + settings.KT_THEME + '/system/not-found.html', status=404)
handler500 = SystemView.as_view(template_name = 'pages/' + settings.KT_THEME + '/system/error.html', status=500)
